package Projects.week4;

public class Pet {
    private String petType;
    private String petName;
    private int petAge;
    private int dogSpace;
    private int catSpace;
    private int daysStay;
    private double amountDue;

    public Pet() {
        String petType = "Unknown";
        String petName = "No Name";
        int dogSpaceNumber = 30;
        int catSpaceNumber = 12;
        int dayStay = 0;
        double amountDue = 0;
    }
    public Pet(String petType, String petName, int dogSpaceNumber, int catSpaceNumber){
        this.petType = petType;
        this.petName = petName;
        petAge = -1;
        this.dogSpace = dogSpaceNumber;
        this.catSpace = catSpaceNumber;
        daysStay = -1;
        amountDue = 0.0;
    }

    public String getPetType() {
        return petType;
    }

    public void setPetType(String petType) {
        this.petType = petType;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public int getPetAge() {
        return petAge;
    }

    public void setPetAge(int petAge) {
        this.petAge = petAge;
    }

    public int getDogSpace() {
        return dogSpace;
    }

    public void setDogSpace(int dogSpace) {
        this.dogSpace = dogSpace;
    }

    public int getCatSpace() {
        return catSpace;
    }

    public void setCatSpace(int catSpace) {
        this.catSpace = catSpace;
    }

    public int getDaysStay() {
        return daysStay;
    }

    public void setDaysStay(int daysStay) {
        this.daysStay = daysStay;
    }

    public double getAmountDue() {
        return amountDue;
    }

    public void setAmountDue(double amountDue) {
        this.amountDue = amountDue;
    }
}