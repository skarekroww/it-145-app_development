package Projects.week2;

public class dog {
    private int dogSpaceNumber;
    private double dogWeight;
    private Boolean grooming;

    public dog(){

    }

    public int getDogSpaceNumber() {
        return dogSpaceNumber;
    }

    public void setDogSpaceNumber(int dogSpaceNumber) {
        this.dogSpaceNumber = dogSpaceNumber;
    }

    public double getDogWeight() {
        return dogWeight;
    }

    public void setDogWeight(double dogWeight) {
        this.dogWeight = dogWeight;
    }

    public Boolean getGrooming() {
        return grooming;
    }

    public void setGrooming(Boolean grooming) {
        this.grooming = grooming;
    }
}
