package Projects.week5andWeek7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Driver {
    private static ArrayList<Dog> dogList = new ArrayList<Dog>();
    private static ArrayList<Monkey> monkeyList = new ArrayList<Monkey>();
    // Instance variables (if needed)

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String userIn;
        Boolean quit = false;
        initializeDogList();
        initializeMonkeyList();
        // Add a loop that displays the menu, accepts the users input
        // and takes the appropriate action.
	// For the project submission you must also include input validation
        // and appropriate feedback to the user.
        // Hint: create a Scanner and pass it to the necessary
        // methods
	// Hint: Menu options 4, 5, and 6 should all connect to the printAnimals() method.

        // i wasn't sure if i should break the while loop after a given command
        // i assumed not. Only because if you do, removing an animal is pointless
        while(!quit){
            displayMenu();
            userIn = scan.nextLine();
            if(userIn.equalsIgnoreCase("1")){
                intakeNewDog(scan);
                //break;
            }else if(userIn.equalsIgnoreCase("2")){
                intakeNewMonkey(scan);
                //break;
            }else if(userIn.equalsIgnoreCase("3")){
                reserveAnimal(scan);
                //break;
            }else if(userIn.equalsIgnoreCase("4")){
                printDogs();
                //break;
            }else if(userIn.equalsIgnoreCase("5")){
                printMonkeys();
                //break;
            }else if(userIn.equalsIgnoreCase("6")){
                printAnimals();
                //break;
            }else if(userIn.equalsIgnoreCase("q")){
                    break;
            }
        }
        return;

    }


    public static void displayMenu() {
        System.out.println("\n\n");
        System.out.println("\t\t\t\tRescue Animal System Menu");
        System.out.println("[1] Intake a new dog");
        System.out.println("[2] Intake a new monkey");
        System.out.println("[3] Reserve an animal");
        System.out.println("[4] Print a list of all dogs");
        System.out.println("[5] Print a list of all monkeys");
        System.out.println("[6] Print a list of all animals that are not reserved");
        System.out.println("[q] Quit application");
        System.out.println();
        System.out.println("Enter a menu selection");

        return;
    }


    public static void initializeDogList() {
        Dog dog4 = new Dog("Sparkles", "Chihuahua", "female", "4", "25.6", "12-12-2019", "Canada", "in service", true, "Canada");
        Dog dog1 = new Dog("Spot", "German Shepherd", "male", "1", "25.6", "05-12-2019", "United States", "intake", true, "United States");
        Dog dog2 = new Dog("Rex", "Great Dane", "male", "3", "35.2", "02-03-2020", "United States", "Phase I", false, "United States");
        Dog dog3 = new Dog("Bella", "Chihuahua", "female", "4", "25.6", "12-12-2019", "Canada", "in service", false, "Canada");

        dogList.add(dog1);
        dogList.add(dog2);
        dogList.add(dog3);
        dogList.add(dog4);

        return;
    }
    public static void initializeMonkeyList() {
        Monkey monkey = new Monkey("sparkles", "species", "tail length", "body height", "body length", "Gender", "Age", "weight", "Date Acquired", "Country Acquired", "Training", true, "canada");
        Monkey monkey2 = new Monkey("Tarnished", "species", "tail length", "body height", "body length", "Gender", "Age", "weight", "Date Acquired", "Country Acquired", "Training", false, "united states");
        Monkey monkey3 = new Monkey("John", "species", "tail length", "body height", "body length", "Gender", "Age", "weight", "Date Acquired", "Country Acquired", "Training", false, "canada");
        Monkey monkey4 = new Monkey("fire", "species", "tail length", "body height", "body length", "Gender", "Age", "weight", "Date Acquired", "Country Acquired", "Training", true, "united states");

        monkeyList.add(monkey);
        monkeyList.add(monkey2);
        monkeyList.add(monkey3);
        monkeyList.add(monkey4);

        return;
    }

    // The input validation to check that the dog is not already in the list
    // is done for you
    public static void intakeNewDog(Scanner scanner) {
        String name = "";
        String breed;
        String gender;
        String age;
        String weight;
        String acquisitionDate;
        String acquisitionCountry;
        String trainingStatus;
        boolean reserved;
        String inServiceCountry;
        boolean end = false;

        //TODO wrap in a loop
        while (!end) {
            System.out.println("What is the dog's name?");
            name = scanner.nextLine();
            for (Dog dog : dogList) {
                if (dog.getName().equalsIgnoreCase(name)) {
                    System.out.println("\n\nThis dog is already in our system\n\n");
                    return;
                }
            }
            System.out.println("What breed is the dog?");
            breed = scanner.nextLine();
            System.out.println("What gender is the dog?");
            gender = scanner.nextLine();
            System.out.println("What age is the dog?");
            age = scanner.nextLine();
            System.out.println("How much does the dog weight?");
            weight = scanner.nextLine();
            System.out.println("When was the dog acquired?");
            acquisitionDate = scanner.nextLine();
            System.out.println("Where was the dog acquired?");
            acquisitionCountry = scanner.nextLine();
            System.out.println("What is the dog's training status?");
            trainingStatus = scanner.nextLine();
            System.out.println("Is the dog reserved?");
            reserved = scanner.nextBoolean();
            System.out.println("Where will " + name + " be servicing people?");
            inServiceCountry = scanner.nextLine();
            inServiceCountry = scanner.nextLine();

            System.out.println(name + " has been added");

            Dog checkIn = new Dog(name, breed, gender, age, weight, acquisitionDate, acquisitionCountry,
                    trainingStatus, reserved, inServiceCountry);
            dogList.add(checkIn);
        }

        return;
    }
	//Instantiate and add the new monkey to the appropriate list
        // For the project submission you must also validate the input
	//to to make sure the monkey doesn't already exist todo and the species type is allowed

    //to be honest, i didn't want to look up a list of monkey species and make a filter on that.
    //ideally i'd Array them, print out the options, and allow use to select one.
        public static void intakeNewMonkey(Scanner scanner) {
        String[] validSpecies = new String[]{"Capuchin","Guenon", "Macaque", "Marmoset", "Squirrel Monkey", "Squirrel", "Tamarin"};
        String name;
        String species = "";
        String tailLength;
        String height;
        String bodyLength;
        String gender;
        String age;
        String weight;
        String ad;
        String ac;
        String training;
        boolean reserved;
        String sc;
        boolean end = false;

        System.out.println("What is the monkey's name?");
        name = scanner.nextLine();

            for (Monkey monkey:monkeyList) {
                if(monkey.getName().equalsIgnoreCase(name)) {
                    System.out.println("\n\n This monkey is in our system already \n\n");
                    return;
                }
            }
            System.out.println("What species is the monkey? Valid Species are.\n");
            for (String valid:validSpecies) {
                System.out.println(valid+" ");
            }
            System.out.println();
            while(!end) {
                species = scanner.nextLine();
                String finalSpecies = species;
                if(Arrays.stream(validSpecies).anyMatch(list -> list.equalsIgnoreCase(finalSpecies))){
                    end = true;
                }else{
                    System.out.println("Again the valid Species are\n");
                    for (String monkey:validSpecies) {
                        System.out.println(monkey+" ");
                    }
                }
            }
            System.out.println("What is the monkey's tail length?");
            tailLength = scanner.nextLine();
            System.out.println("What is the monkey's height?");
            height = scanner.nextLine();
            System.out.println("What is the monkey's body length?");
            bodyLength = scanner.nextLine();
            System.out.println("What is the monkey's gender?");
            gender = scanner.nextLine();
            System.out.println("What is the monkey's age?");
            age = scanner.nextLine();
            System.out.println("What is the monkey's Weight?");
            weight = scanner.nextLine();
            System.out.println("When did you acquire "+ name+"?");
            ad = scanner.nextLine();
            System.out.println("What country did you acquire"+ name+"?");
            ac = scanner.nextLine();
            System.out.println("What is the monkey's training status?");
            training = scanner.nextLine();
            System.out.println("Is "+ name + " reserved?");
            reserved = scanner.nextBoolean();
            System.out.println("what country is the monkey serving in?");
            sc = scanner.nextLine();
            sc = scanner.nextLine();

            Monkey monkey = new Monkey(name, species, tailLength, height,
                    bodyLength, gender, age, weight, ad, ac, training, reserved, sc);
            monkeyList.add(monkey);

            return;
        }

        // You will need to find the animal by animal type and in service country
        public static void reserveAnimal(Scanner scanner) {
            boolean end = false;
            String available;


            System.out.println("Which animal are you looking to reserve? [1] Dog or [2] monkey?");
            String checkOut = scanner.nextLine();
            boolean theEnd = false;
            System.out.println("What country are you from?");
            String country = scanner.nextLine();

//todo wrap this in a while loop an animal is checked out

            if(checkOut.equalsIgnoreCase("dog")|| checkOut.equalsIgnoreCase("1")){
                System.out.println("These dogs are able to be reserved based on location\n\n");
                //Getting a list of available dogs
                available = dogList.stream().filter(list -> list.getReserved() == false).filter(list -> list.getInServiceLocation().equalsIgnoreCase(country)).map(list ->list.getName() +" Is a "+ list.getBreed()).collect(Collectors.joining("\n"));
                //prints said list
                System.out.println(available);
                //Asking to make pick of dog to change their status
                System.out.println("\nWhich would you like to reserve? (By name)");
                checkOut = scanner.nextLine();
                //Changes status if dog is chosen.
                for (Dog dog:dogList) {
                    if(dog.getName().equalsIgnoreCase(checkOut)){
                        System.out.println(dog.getName() + " has been reserved");
                    }
                }
                //Checks status (mainly for myself making sure im right.)
//                for (Dog dog:dogList) {
//                    System.out.println(dog.getName() + " " +dog.getReserved());
//                }
            }
            else if(checkOut.equalsIgnoreCase("monkey") || checkOut.equalsIgnoreCase("2")){
                System.out.println("Theses are the monkeys available based on your location \n");
                //This will list out all monkeys available (names and species)
                available = monkeyList.stream().filter(list -> list.getReserved() == false).filter(list -> list.getInServiceLocation().equalsIgnoreCase(country)).map(list ->list.getName() +" Is a "+ list.getSpecies()).collect(Collectors.joining("\n"));
                System.out.println(available);
                //input the monkeys name to check out
                System.out.println(" \nWhich monkey would you like to reserve? (By name)");
                checkOut = scanner.nextLine();

                //This will check out the named monkey
                for (Monkey monkey:monkeyList) {
                    if(monkey.getName().equalsIgnoreCase(checkOut)){
                        monkey.setReserved(true);
                        System.out.println(monkey.getName() + " has been reserved");
                    }
                }
                // to make sure monkey has been reserved
//                for (Monkey monkey:monkeyList) {
//                    System.out.println(monkey.getName() + " reserved status " + monkey.getReserved());
//                }

            }else{
                System.out.println("Please either enter Monkey or Dog.");
            }
            return;
        }



        public static void printDogs() {
            for (Dog dog:dogList) {
                System.out.println(dog.toString());
            }
            return;
        }
        public static void printMonkeys(){
            for (Monkey monkey:monkeyList) {
                System.out.println(monkey.toString());
            }
            return;
        }
        public static void printAnimals() {
            System.out.println("List of Dogs not reserved\n");
            //stream acts as For Each
            //Filter will go through everything's (get)Reserved status,
            // if they're set true remove from array
            //Else continue to the map
            //map will alter what we want.
            // Now will go through the list we have (after we filtering out reserved animals) and add them to the respected Object (*animal*NotReserved) spaced out by a new line
            String dogsNotReserved = dogList.stream().filter(list -> list.getReserved() != true).map(list -> list.toString()).collect(Collectors.joining("\n"));
            String monkeysNotReserved = monkeyList.stream().filter(list -> list.getReserved() != true).map(list -> list.toString()).collect(Collectors.joining("\n"));
            System.out.println(dogsNotReserved);
            System.out.println("\nList of Monkeys not reserved\n");
            System.out.println(monkeysNotReserved);

            return;
        }
}

