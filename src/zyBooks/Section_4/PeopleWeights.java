package zyBooks.Section_4;

import java.util.Scanner;

public class PeopleWeights {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double total = 0;
        double average = 0;
        double max = 0;
//        double[] weight = {236.0, 89.5, 142.0, 166.3, 93.0};
        String entered = "";
        double[] weight = new double[5];

        for(int x = 0; x < 5; x++){
            System.out.println("Enter weight "+(x+1)+": ");
            weight[x] = scan.nextDouble();
            if(weight[x] > max){
                max = weight[x];
            }
            total += weight[x];
        }
        for (double y:weight) {
            if(y > max){
                max = y;
            }
        }
        System.out.println();
        System.out.print("You entered: ");
        for (int i = 0; i < weight.length; i++) {
            System.out.print(weight[i]+" ");
        }
        System.out.println();
        System.out.println("Total weight: "+ total);
        System.out.println("Average weight: "+ (total / 5));
        System.out.println("Max weight: "+max);

        return;
    }
}