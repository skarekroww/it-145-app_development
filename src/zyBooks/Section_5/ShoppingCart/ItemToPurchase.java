package zyBooks.Section_5.ShoppingCart;

public class ItemToPurchase {
    //Private fields - itemName, itemPrice, and itemQuanity
    private String itemName;
    private int itemPrice;
    private int itemQuantity;

    public void ItemToPurchase(){
        itemName = "none";
        itemPrice = 0;
        itemQuantity = 0;
    }

    public String getName() {
        return itemName;
    }

    public void setName(String itemName) {
        this.itemName = itemName;
    }

    public int getPrice() {
        return itemPrice;
    }

    public void setPrice(int itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getQuantity() {
        return itemQuantity;
    }

    public void setQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    //print item to purchase
    public void printItemPurchase() {
        System.out.println(itemName +" "+ itemQuantity + " @ $"+itemPrice +" = $"+ (itemPrice*itemQuantity));
    }
    public int total(){
        return (itemPrice * itemQuantity);
    }
}
