package zyBooks.Section_6.PraseString;

import java.util.Scanner;

public class ParseStrings {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userInput = "";
        String [] userArray;
        boolean end = false;

        while (!end) {
            System.out.print("Enter input string: \n");
            userInput = scanner.nextLine();

            if (userInput.equals("q")){
                end = true;
                break;
            }
            if (!userInput.contains(",")) {
                System.out.println("Error: No comma in string");
                continue;
            }else {
                userArray = userInput.split(",");
                System.out.println("First word: " + userArray[0].trim());
                System.out.println("Second word: " + userArray[1].trim());
                System.out.println();
                System.out.println();
            }
        }
        return;
    }
}