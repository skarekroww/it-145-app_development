package zyBooks.Section_2;

import java.util.Scanner;

public class LabProgram {

    public static double drivingCost(double milesPerGallon, double dollarsPerGallon, double milesDriven){
        double total = (milesDriven / milesPerGallon) * dollarsPerGallon;
        return total;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double mpg = scan.nextDouble();
        double dollarsPerGallon = scan.nextDouble();

        System.out.printf("%.2f ", drivingCost(mpg, dollarsPerGallon, 10.0));
        System.out.printf("%.2f ", drivingCost(mpg, dollarsPerGallon, 50.0));
        System.out.printf("%.2f\n", drivingCost(mpg, dollarsPerGallon, 400.0));

        return;
    }
}
