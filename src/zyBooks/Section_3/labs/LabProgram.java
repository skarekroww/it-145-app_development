package zyBooks.Section_3.labs;

import java.util.Scanner;

public class LabProgram {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        String userText;
        int total=0;
        // Add more variables as needed

        userText = scnr.nextLine();  // Gets entire line, including spaces.

        for(int x = 0; x < userText.length(); x++){
            char current = userText.charAt(x);
            if(current != ' ' && current != ',' && current != '.' && current != '!'){
                total += 1;
            }
        }
        System.out.println(total);
        return;
    }
}
