package zyBooks.Section_3.labs;

import java.util.Scanner;

public class DrawHalfArrow {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        int arrowBaseHeight;
        int arrowBaseWidth;
        int arrowHeadWidth;

        System.out.println("Enter arrow base height:");
        arrowBaseHeight = scnr.nextInt();

        System.out.println("Enter arrow base width:");
        arrowBaseWidth = scnr.nextInt();

        System.out.println("Enter arrow head width:");
        arrowHeadWidth = scnr.nextInt();

        while (arrowHeadWidth <= arrowBaseWidth) {
            System.out.println("Enter arrow head width:");
            arrowHeadWidth = scnr.nextInt();
        }
        System.out.println("");

        for(int x = 0; x < arrowBaseHeight; x++){
            for(int y = 0; y < arrowBaseWidth; y++){
                System.out.print("*");
            }
            System.out.println("");
        }
        for(int x = arrowHeadWidth; x > 0; x--){
            for(int y = 0; y < x; y++){
                System.out.print('*');
            }
            System.out.println("");
        }
        return;
    }
}