package zyBooks.Section_1;

import java.util.Scanner;

public class OutputWithVars {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        int userNum = 0;

        System.out.println("Enter integer: ");
        userNum = scnr.nextInt();

        System.out.println("You entered: " + userNum);

        System.out.println(userNum + " squared is " + (int)Math.abs(Math.pow(userNum, 2)) );
        System.out.println("And "+ userNum + " cubed is " +  (int)Math.pow(userNum, 3) +"!!");

        System.out.println("Enter another integer: ");
        int userNum2 = scnr.nextInt();
        System.out.println(userNum + " + " + userNum2 + " is " + (userNum + userNum2) );
        System.out.println(userNum + " * " + userNum2 + " is " + userNum * userNum2 );

        return;
    }
}